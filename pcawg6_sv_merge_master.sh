#!/bin/bash
#baseDir="/Users/"
#source activate py27
#DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DIR=${HOME}
echo $DIR

aliquot_id=$1

#DRANGER_VCF=$(find $DRANGER_DIR -iname ${aliquot_id}*.somatic.sv.vcf.gz)
#SNOWMAN_VCF=$(find $SNOWMAN_DIR -iname ${aliquot_id}*.somatic.sv.vcf.gz)
#SANGER_VCF=$(find $BRASS_DIR  -iname ${aliquot_id}*.annot.vcf.gz)
#DELLY_VCF=$(find $DELLY_DIR -iname ${aliquot_id}*sv.vcf.gz)

DRANGER_VCF=$2
SNOWMAN_VCF=$3
SANGER_VCF=$4
DELLY_VCF=$5

today=$(date +"%y%m%d")

ETC_PCAWG6_DIR=/etc/pcawg6_merge_sv

source <(grep = ${ETC_PCAWG6_DIR}/merge.ini)

DATA_DIR="${DIR}/data"

if [[ -z ${ANALYSIS_DIR} ]]
then
ANALYSIS_DIR="${DIR}/output"
#ANALYSIS_DIR="${DATA_DIR}/${today}.sv_merge"
fi
if [[ ! -d ${ANALYSIS_DIR}  ]]
then
mkdir -p $ANALYSIS_DIR
fi

CODE_DIR="/opt/scripts"
VARIANT_DIR="${ANALYSIS_DIR}/sv_call_concordance"
DRANGER_DIR=${DIR}/${DRANGER}
SNOWMAN_DIR=${DIR}/${SNOWMAN}
BRASS_DIR=${DIR}/${BRASS}
DELLY_DIR=${DIR}/${DELLY}
#DRANGER_DIR="${DATA_DIR}/dranger.160503"
#SNOWMAN_DIR="${DATA_DIR}/snowman.160503"
#BRASS_DIR="${DATA_DIR}/brass.160404"
#DELLY_DIR="${DATA_DIR}/delly.160418"
# back in black...
PCAWG6_DATA_DIR="${ETC_PCAWG6_DIR}/data"
BLACKLIST_DIR="${PCAWG6_DATA_DIR}/blacklist_files"
BLACKLIST_BEDPE="${BLACKLIST_DIR}/pcawg6_blacklist.slop.bedpe.gz"
BLACKLIST_BED="${BLACKLIST_DIR}/pcawg6_blacklist.slop.bed.gz"
BLACKLIST_TE_BEDPE="${BLACKLIST_DIR}/pcawg6_blacklist_TE_pseudogene.bedpe.gz"
BLACKLIST_TE_INS="${BLACKLIST_DIR}/pcawg6_blacklist_TE_pseudogene_insertion.txt.gz"
BLACKLIST_ALIQUOT_ID="${BLACKLIST_DIR}/blacklist_aliquot_id.txt"
BLACKLIST_FOLDBACK="${BLACKLIST_DIR}/pcawg6_blacklist_foldback_artefacts.slop.bedpe.gz"
BLACKLIST_ALIQUOT_DIR=${ANALYSIS_DIR}/blacklisted
if [[ ! -d ${BLACKLIST_ALIQUOT_DIR} ]]
then
mkdir $BLACKLIST_ALIQUOT_DIR
fi
CCDSGENE="${BLACKLIST_DIR}/ccdsGene.bed.gz"
PCAWG_QC="${PCAWG6_DATA_DIR}/PCAWG-QC_Summary-of-Measures.tsv"
pcawg_dataset="${PCAWG6_DATA_DIR}/pcawg_release_mar2016.tsv"
log=${ANALYSIS_DIR}/${aliquot_id}.${call_stamp}.${today}.log

echo -en "" > ${log}
PROJECT=$(grep $aliquot_id $pcawg_dataset | cut -f 2)
if [[  -z $PROJECT  ]];then
PROJECT="UNKOWN"
fi

SAMPLE_DIR=${VARIANT_DIR}/${PROJECT}/${aliquot_id}

mkdir -p $SAMPLE_DIR
cd $SAMPLE_DIR
echo -e "########################################\n\n"$PROJECT"\naliquot id=" $aliquot_id "\nsv merge set" ${call_stamp} "\nmerged on" $(date)  | tee  $log

#####################################################################
######################### REFORMAT TO BEDPE #########################
#####################################################################
echo -e "\n## 1)\tREFORMAT TO BEDPE\n"

echo -e "## BROAD ##"


if [[ ! -z $DRANGER_VCF ]]
then
DRANGER_ANNO_VCF=${DRANGER_VCF/.vcf.gz/_annoTmp.vcf.gz}
zcat $DRANGER_VCF | awk 'BEGIN{OFS="\t"}{if ($1~/^#/) print; else print $1"\t"$2"\tdRANGER_"$3, $4,$5,$6,$7,$8,$9,$10,$11}' | grep -v '\--->'| bgzip -c > $DRANGER_ANNO_VCF
DRANGER_BEDPE=${DRANGER_ANNO_VCF/.vcf.gz/.sv.bedpe}
python  ${CODE_DIR}/dRangerVcf2Bedpe.py $DRANGER_ANNO_VCF ${DRANGER_BEDPE}
echo vcf file $DRANGER_VCF | tee -a $log
wc -l ${DRANGER_BEDPE}  | tee -a $log

else
echo dranger file not preset  | tee -a $log
fi

echo -e "## SNOWMAN ##"

if [[ ! -z $SNOWMAN_VCF ]]
then
SNOWMAN_ANNO_VCF=${SNOWMAN_VCF/.vcf.gz/_annoTmp.vcf.gz}
zcat $SNOWMAN_VCF | awk 'BEGIN{OFS="\t"}{if ($1~/^#/) print; else print $1"\t"$2"\tSNOWMAN_"$3,  $4,$5,$6,$7,$8,$9,$10,$11}' | grep -v '\--->' | bgzip -c > $SNOWMAN_ANNO_VCF
SNOWMAN_BEDPE=${SNOWMAN_ANNO_VCF/.vcf.gz/.sv.bedpe}
python  ${CODE_DIR}/snowmanVcf2Bedpe.py ${SNOWMAN_ANNO_VCF} ${SNOWMAN_BEDPE}
echo vcf file $SNOWMAN_VCF | tee -a $log
wc -l ${SNOWMAN_BEDPE}  | tee -a $log

else
echo snowman file not present  | tee -a $log
fi

echo -e "## SANGER ##"


if [[ ! -z $SANGER_VCF ]]
then
SANGER_ANNO_VCF=${SANGER_VCF/.vcf.gz/_annoTmp.vcf.gz}
zcat $SANGER_VCF | awk 'BEGIN{OFS="\t"}{if ($1~/^#/) print; else print $1"\t"$2"\tBRASS_"$3, $4,$5,$6,$7,$8,$9,$10,$11}' | grep -v '\--->' | bgzip -c > $SANGER_ANNO_VCF
BRASS_BEDPE=${SANGER_ANNO_VCF/.vcf.gz/.sv.bedpe}
python ${CODE_DIR}/SangerVcf2Bedpe.py $SANGER_ANNO_VCF ${BRASS_BEDPE}
echo vcf file $SNOWMAN_VCF | tee -a $log
wc -l ${BRASS_BEDPE}  | tee -a $log

else
echo brass file not present  | tee -a $log
fi

echo -e "## EMBL ##"

# DELLY_ANNO_VCF=$DELLY_VCF
if [[ ! -z $DELLY_VCF ]]
then
DELLY_ANNO_VCF=${DELLY_VCF/.vcf.gz/_annoTmp.vcf.gz}
zcat  $DELLY_VCF | awk 'BEGIN{FS="\t"}{if ($1~/^#/) print; else print $1"\t"$2"\tDELLY_"$3, $4,$5,$6,$7,$8,$9,$10,$11}' | grep -v '\--->' | bgzip -c > $DELLY_ANNO_VCF

DELLY_BEDPE=${DELLY_ANNO_VCF/.vcf.gz/.sv.bedpe}
python ${CODE_DIR}/dellyVcf2Bedpe.py $DELLY_ANNO_VCF ${DELLY_BEDPE}
echo vcf file $DELLY_VCF | tee -a $log
wc -l ${DELLY_BEDPE}  | tee -a $log

else
echo delly file not present | tee -a $log
fi

#####################################################################
######################### MAKE MASTER FILE  #########################
#####################################################################
echo -e "\n## 2)\tCOMBINE BEDPE SV CALLS\n"

bedpe_header="chrom1\tstart1\tend1\tchrom2\tstart2\tend2\tname\tqual\tstrand1\tstrand2\tsv_class\tsv_type\tscore\tsupp_reads\tscna\tcenter\tread_id"

########## make master list #####################

SV_MASTER=${SAMPLE_DIR}/masterSvList.bed

echo -e $bedpe_header > $SV_MASTER
cat ${BRASS_BEDPE} ${DRANGER_BEDPE} ${SNOWMAN_BEDPE} ${DELLY_BEDPE} | sort -k1,1V | uniq | grep -v chrom1 >> $SV_MASTER

echo -e "\n$SV_MASTER" | tee -a $log

#####################################################################
#######################      PAIR the SVs   #########################
#####################################################################
echo -e "\n## 2.1)\tPAIR the SVs\n"
SLOP=400
PAIR2PAIR=${SAMPLE_DIR}/pair2pair_SV_merging.bed
echo -n "" > $PAIR2PAIR
for bed in ${BRASS_BEDPE} ${DRANGER_BEDPE} ${SNOWMAN_BEDPE} ${DELLY_BEDPE}
do
pairToPair  -slop $SLOP -rdn -a <(cut -f -16 ${SV_MASTER}) -b <(cut -f -16  $bed | awk -v aliquot_id=$aliquot_id   '{print $0"\t"aliquot_id}' ) >> $PAIR2PAIR
done

#####################################################################
############ Make SV overlap for each SV in pair2pair ###############
#####################################################################
echo -e "\n## 2.2)\tMake SV overlap for each SV in pair2pair\n"
## prepare special data frame format to load into graph algorithm

inBEDPE=$SAMPLE_DIR/${aliquot_id}_SV_overlap.txt
python ${CODE_DIR}/pcawg_merge_reorder_pairs.py $PAIR2PAIR $aliquot_id > ${inBEDPE}

#####################################################################
#############	  Merge and get cliques of SVs 	    #################
#####################################################################
echo -e "\n## 3)\tMERGE SVs\n"

outVCF=$ANALYSIS_DIR/${aliquot_id}.${call_stamp}.${today}.somatic.sv.vcf

outSTAT=${outVCF/.vcf/.stat}

cd ${SAMPLE_DIR}

python ${CODE_DIR}/pcawg6_sv_merge_graph.py -e ${inBEDPE} -o ${outVCF} -s ${outSTAT} -a $SANGER_ANNO_VCF -b $DELLY_ANNO_VCF -c $DRANGER_ANNO_VCF -d $SNOWMAN_ANNO_VCF | tee -a $log

#####################################################################
##############    RNA/GERMLINE BLACKLIST  REMOVAL   #################
#####################################################################
echo -e "\n\tREMOVE BLACKLIST\n"

BLACKLIST_SV_ID=${aliquot_id}.blacklist_svid.txt
echo -en "" > ${BLACKLIST_SV_ID}
## remove SVs with both breaks overlapping blacklist regions - germline artefacts and TEs
pairToPair -is -type both -a ${outVCF/.vcf/.bedpe} -b $BLACKLIST_BEDPE  > ${outVCF/.vcf/_blacklisted_tmp.bedpe}
if [[ -s ${outVCF/.vcf/_blacklisted_tmp.bedpe}  ]];then
cut -f 7,19 ${outVCF/.vcf/_blacklisted_tmp.bedpe} | sed 's/|.*//' | sort -u   >> ${BLACKLIST_SV_ID}
fi

## remove SVs with both breaks overlapping blacklist regions and read orientation match - foldback inversions
pairToPair -type both -a ${outVCF/.vcf/.bedpe} -b $BLACKLIST_FOLDBACK  > ${outVCF/.vcf/_blacklisted_tmp.bedpe}
if [[ -s ${outVCF/.vcf/_blacklisted_tmp.bed}  ]];then
cut -f 7,18 ${outVCF/.vcf/_blacklisted_tmp.bed} | sed 's/|.*//' | sort -u   >> ${BLACKLIST_SV_ID}
fi

## remove SVs with one break overalpping blacklist bed file regions
pairToBed -a ${outVCF/.vcf/.bedpe}  -b ${BLACKLIST_BED}  > ${outVCF/.vcf/_blacklisted_tmp.bed}
if [[ -s ${outVCF/.vcf/_blacklisted_tmp.bed}  ]];then
cut -f 7,18 ${outVCF/.vcf/_blacklisted_tmp.bed} | sed 's/|.*//' | sort -u   >> ${BLACKLIST_SV_ID}
fi
## remove SVs with either break in pseudogene exon-exon region
pairToPair -is -type either -a ${outVCF/.vcf/.bedpe}  -b ${BLACKLIST_TE_BEDPE} > ${outVCF/.vcf/_blacklisted_TE_pseudogene_tmp.bed}
if [[ -s ${outVCF/.vcf/_blacklisted_TE_pseudogene_tmp.bed}  ]];then
cut -f 7,19 ${outVCF/.vcf/_blacklisted_TE_pseudogene_tmp.bed} | sed 's/|.*//' | sort -u   >> ${BLACKLIST_SV_ID}
fi
echo -e "\n## 4)\tRNA CONTAMINATION FILTER\n"

## RNA
bash ${CODE_DIR}/remove_splicing_type_svs_yl.sh ${outVCF/.vcf/.bedpe} ${CCDSGENE} ${BLACKLIST_TE_INS}  > splice_type_id_blacklist.txt
if [[ -s splice_type_id_blacklist.txt  ]];then
awk '{print $1"\tcdna_contamination"}' splice_type_id_blacklist.txt >> ${BLACKLIST_SV_ID}
fi
rm splice_type_id_blacklist.txt
## FILTER and ANNOTATE
python ${CODE_DIR}/pcawg6_sv_merge_annotate.py ${outVCF} ${BLACKLIST_SV_ID}  ${aliquot_id} ${PCAWG_QC} | tee -a $log

echo -e "\n\ngenerate BEDPE fil"
python ${CODE_DIR}/pcawg6Vcf2Bedpe.py ${outVCF}
python ${CODE_DIR}/pcawg6Vcf2Bedpe.py ${outVCF/.vcf/_full.vcf}

bgzip -f $outVCF

if [[ -f ${outVCF/.vcf/_tmp.vcf}  ]];then
bgzip -f ${outVCF/.vcf/_tmp.vcf}
fi

if [[ -f ${outVCF/.vcf/_full.vcf}  ]];then
bgzip -f ${outVCF/.vcf/_full.vcf}
fi

echo -e "\n\nnumber of merged SVs" | tee -a  $log
zgrep -vP "^#"  $outVCF | awk '$3~/_1/' | wc -l | tee -a  $log
tabix -f -p vcf ${outVCF}.gz
tabix -f -p vcf ${outVCF/.vcf/_full.vcf}.gz
cd -

if [[ $(stat -c %s ${outVCF}.gz) -gt 100  ]]; then echo -e "\n\n>>\t" $aliquot_id vcf file successful"\n>>\t${outVCF}.gz\n"; else echo -e ">>\t"  $aliquot_id generation error;fi  | tee -a $log

echo -e "output clique count: ${aliquot_id}_SV_overlap.clique.txt"
echo -e "output clique stat: ${aliquot_id}.stats"

mv -f ${ANALYSIS_DIR}/${aliquot_id}*tmp*  $SAMPLE_DIR
mv -f ${ANALYSIS_DIR}/${aliquot_id}*txt  $SAMPLE_DIR
mv -f ${ANALYSIS_DIR}/${aliquot_id}*no_exon_exon_artefacts* $SAMPLE_DIR

if grep ${aliquot_id} ${BLACKLIST_ALIQUOT_ID} > /dev/null;then
echo -e "\n>>> sample blacklisted. Will be excluded from the set <<<\n"
mv ${ANALYSIS_DIR}/${aliquot_id}* ${BLACKLIST_ALIQUOT_DIR}
fi

echo -e "complete, log file\n\n$log"
#exit 0
#####################################################################
#####	Rearrange clique calls and make binary tree for plotting ####
#####################################################################

clique=$SAMPLE_DIR/${aliquot_id}_SV_overlap.clique.txt
cliqueCenter=${clique/.txt/.center.txt}
echo -ne "" > ${cliqueCenter};
for i in $(cut -f1 ${clique});
do

if echo $i |grep -P '^BRASS'  > /dev/null;
then center=brass;

elif echo $i | grep -P '^dRANGER'  > /dev/null;
then center=dranger;
elif echo $i | grep -P '^SNOWMAN'  > /dev/null;
then center=snowman;

elif echo $i  | grep -P '^DELLY'  > /dev/null;
then center=delly;
fi; awk -v id=$i '$1==id' ${clique} | sed "s/$i/$center/"  >> ${cliqueCenter};
done

### COUNT TOTAL SVs

dranger_count=$(grep -v "chrom1" $DRANGER_BEDPE  | wc -l)
delly_count=$(grep -v "chrom1" $DELLY_BEDPE |   wc -l)
brass_count=$(grep -v "chrom1" $BRASS_BEDPE |  wc -l)
snowman_count=$(grep -v "chrom1" $SNOWMAN_BEDPE |  wc -l)

## Plotting

binaryOut=$SAMPLE_DIR/${aliquot_id}.binaryTree.txt
echo $binaryOut
cat <(echo -e "brass\ndelly\ndranger\nsnowman") <(sort -k2,2n ${cliqueCenter}) |\
awk  'BEGIN{OFS="\t";print "clique\tbrass\tdelly\tdranger\tsnowman" ;sampleCount=0; clusterId=0; }{ if(NF==1) { sampleCount++; samples[$1]=sampleCount; sampleOccur[sampleCount]=0; } else { if ($2==clusterId) { sampleOccur[samples[$1]]=1; } else { printf clusterId; for (i=1; i<=sampleCount; i++) { printf "\t"sampleOccur[i]; sampleOccur[i]=0; } print ""; clusterId=$2; sampleOccur[samples[$1]]=1;}  }} END { printf clusterId; for (i=1; i<=sampleCount; i++) { printf "\t"sampleOccur[i] } print ""}' | awk '$1!=0' > $binaryOut

echo -e "count\t$brass_count\t$delly_count\t$dranger_count\t$snowman_count" >> $binaryOut
Rscript ${CODE_DIR}/pcawg_multiple_sv_merging_heatmap.R $binaryOut

echo -e "merged SV file:\n${outVCF}.gz\n" | tee -a $log
