#!/usr/bin/env cwl-runner

class: CommandLineTool
id: pcawg_sv_merge
label: pcawg_sv_merge_workflow
cwlVersion: v1.0

doc: |
    ![build_status](https://img.shields.io/docker/build/weischenfeldt/pcawg_sv_merge.svg)
    A Docker container for the pcawg_sv_merge_workflow.

dct:creator:
  '@id': http://orcid.org/0000-0003-3684-2659
  foaf:name: Francesco Favero
  foaf:mbox: francesco.favero@bric.ku.dk

dct:contributor:
  foaf:name: Etsehiwot Girum Girma
  foaf:mbox: Etsehiwot@finsenlab.dk

requirements:
  - class: DockerRequirement
    dockerPull: registry.hub.docker.com/weischenfeldt/pcawg_sv_merge:1.0.2

hints:
  - class: ResourceRequirement
    coresMin: 2
    ramMin: 4092
    outdirMin: 512000

inputs:
  run_id:
    type: string
    inputBinding:
      position: 1
      prefix: --run-id
  dranger:
    type: File
    inputBinding:
      position: 2
      prefix: --dranger
  snowman:
    type: File
    inputBinding:
      position: 3
      prefix: --snowman
  brass:
    type: File
    inputBinding:
      position: 4
      prefix: --brass
  delly:
    type: File
    inputBinding:
      position: 5
      prefix: --delly
outputs:
  log:
    type: File
    outputBinding:
      glob: 'output/*.log'
  somatic_sv_bedpe:
    type: File
    outputBinding:
      glob: 'output/*.somatic.sv.bedpe'
  somatic_sv_full_bedpe:
    type: File
    outputBinding:
      glob: 'output/*.somatic.sv_full.bedpe'
  somatic_sv_full_vcf:
    type: File
    outputBinding:
      glob: 'output/*.somatic.sv_full.vcf.gz'
  somatic_sv_full_tbi:
    type: File
    outputBinding:
      glob: 'output/*.somatic.sv_full.vcf.gz.tbi'
  somatic_sv_tbi:
    type: File
    outputBinding:
      glob: 'output/*.somatic.sv.vcf.gz.tbi'
  somatic_sv_vcf:
    type: File
    outputBinding:
      glob: 'output/*.somatic.sv.vcf.gz'
  somatic_sv_stat:
    type: File
    outputBinding:
      glob: 'output/*.somatic.sv.stat'
  sv_tar:
    type: File
    outputBinding:
      glob: 'output/*.tar.gz'


baseCommand: [/usr/bin/run_sv_merge.py]
