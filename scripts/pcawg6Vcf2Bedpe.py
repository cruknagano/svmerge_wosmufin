#!/usr/bin/env python
## created: 20160720
## by: Joachim Weischenfeldt
'''
python pcawg_sv_merge/pcawg6Vcf2Bedpe.py file.vcf ['opt_field']

convert vcf file to BEDPE format
optional field, e.g. "TE_origin"
'''

from __future__ import print_function
import os,sys,re,vcf, gzip, csv, re

from collections import defaultdict

vcfFile = sys.argv[1]
optField = ""
if len(sys.argv) < 2:
    print ('syntax: <0> <vcf>')
    sys.exit(-1)
if len(sys.argv) == 2:
    bedpeFile = re.sub(r'.vcf.*', r'.bedpe', vcfFile)
else:
    bedpeFile = sys.argv[2]
if len(sys.argv) == 4:
    optField = sys.argv[3]
    print ('provided optional info field ', optField, '\nWill try to parse from vcf')

if not 'vcf' in vcfFile:
    print ('file must contain vcf* suffix')
    sys.exit( -1)

print ("input:", vcfFile)

header = ["chrom1", "start1", "end1", "chrom2","start2","end2", "sv_id","pe_support","strand1","strand2","svclass","svmethod"]
with open(bedpeFile, 'w') as wout:
    writer = csv.writer(wout, delimiter="\t", lineterminator="\n")
    reader = vcf.Reader(open(vcfFile), 'r', compressed=True) if vcfFile.endswith('.gz') else vcf.Reader(open(vcfFile), 'r', compressed=False)
    if not optField in reader.infos.keys():
        print ("optional field",optField, "not present in vcf")
    else:
        header = header + [optField]
    writer.writerow(header)
    sv_dict = defaultdict(dict)
    for record in reader:
        svid = re.sub(r'_[12]$', '', record.ID)
        strand1, strand2 = record.INFO['STRAND'], record.INFO['MATESTRAND']
        pe_support = record.INFO["PE"]
        svclass = record.INFO["SVCLASS"][0]
        svmethod = record.INFO["SVMETHOD"]
        if not optField:
            svinfo = [svid, pe_support, strand1, strand2, svclass, svmethod]
        else:
            opt_info = ["."]
            if optField in record.INFO:
                opt_info = record.INFO[optField]
            svinfo = [svid, pe_support, strand1, strand2, svclass, svmethod] + opt_info
        if record.ID.endswith('_1'):
            coord1 = [record.CHROM, record.POS, int(record.POS)+1]
            sv_dict[svid]["coord1"] = coord1
            sv_dict[svid]["svinfo"] = svinfo
        if record.ID.endswith('_2'):
            coord2 = [record.CHROM, record.POS, int(record.POS)+1]
            sv_dict[svid]["coord2"] = coord2
    bedpe = list()
    for svkey in sv_dict.keys():
        bedpe.append( sv_dict[svkey]['coord1'] +  sv_dict[svkey]['coord2'] + sv_dict[svkey]['svinfo'])
    bedpe.sort()
    writer.writerows(bedpe)


print ("\n\nwrote to", bedpeFile)

