# wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/database/ccdsGene.txt.gz
# gunzip ccdsGene.txt
process_ccdsgene='
    while (<>) {
        chomp;
        @F = split /\t/;
        $F[2] =~ s/^chr//;
        @exon_starts = split /,/, $F[9];
        @exon_ends   = split /,/, $F[10];
        $cds_start = $F[6];
        $cds_end = $F[7];
        $i = 0;
        for (0..$#exon_starts) {
            if ($exon_starts[$_] <= $cds_end && $exon_ends[$_] >= $cds_start) {
                print join(
                    "\t",
                    $F[2],
                    $exon_starts[$_],
                    $exon_ends[$_],
                    "$F[1].$i",
                    $F[1],
                    $F[3],
                );
                print "\n";
                $i++;
            }
        }
    }
'
perl -e "$process_ccdsgene" ccdsGene.txt > ccdsGene.bed

